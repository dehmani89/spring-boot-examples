package com.onfs.agent.registration.controller;

import java.util.List;

import com.onfs.agent.registration.dto.Person;
import com.onfs.agent.registration.service.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LdapBindController {

    @Autowired
    private PersonRepo personRepo;

    @PostMapping("/add-user")
    public ResponseEntity<String> bindLdapPerson(@RequestBody Person person) {
        String result = personRepo.create(person);

        ResponseEntity responseEntity = new ResponseEntity<>(result, HttpStatus.OK);

        System.out.println("statuse code = " + responseEntity.getStatusCode());
        System.out.println("statuse code = " + responseEntity.getStatusCode().getReasonPhrase());
        System.out.println("statuse code = " + responseEntity.getStatusCodeValue());

        if(responseEntity.getStatusCodeValue() == 200){
            return responseEntity;
        }else{
            result = "Error Adding a User";
            return new ResponseEntity<>(result, responseEntity.getStatusCode());
        }
    }

    @PutMapping("/update-user")
    public ResponseEntity<String> rebindLdapPerson(@RequestBody Person person) {
        String result = personRepo.update(person);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/retrieve-users")
    public ResponseEntity<List<Person>> retrieve() {
        return new ResponseEntity<List<Person>>(personRepo.retrieve(), HttpStatus.OK);
    }

    @DeleteMapping("/remove-user")
    public ResponseEntity<String> unbindLdapPerson(@RequestParam(name = "userId") String userId) {
        String result = personRepo.remove(userId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}