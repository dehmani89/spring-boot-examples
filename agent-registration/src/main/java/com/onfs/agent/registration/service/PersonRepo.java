package com.onfs.agent.registration.service;

import com.onfs.agent.registration.dto.Person;

/**
 * service layer, which performs CRUD operation on LDAP records using LdapTemplate.
 */
import java.util.List;

public interface PersonRepo {

    public List<Person> retrieve();
    public String create(Person p);
    public String update(Person p);
    public String remove(String userId);
}