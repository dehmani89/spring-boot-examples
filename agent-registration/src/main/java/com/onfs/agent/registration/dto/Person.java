package com.onfs.agent.registration.dto;

/**
 * This DTO class will be used to hold the LDAP record during the CRUD operation.
 */

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@Data
public class Person {

    private String userId;

    @NotNull(message = "fullName can not be null!!")
    @NotEmpty(message = "fullName can not be empty!!")
    private String fullName;

    private String lastName;

    private String description;
}